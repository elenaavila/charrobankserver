const crypt = require('../utils/Crypt');
const userSrv = require('../services/UsersService');

const mLabUrl = process.env.MLAB_API_URL;
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function loginV1(req, res){
    console.log("POST /charrobank/v1/login");

    var email = req.body.email;
    var pass = req.body.password;

    if(!email || !pass){ //Parametros incorrectos
      return res.status(400).send({"mensaje":"Login incorrecto"});
    }

    console.log("El email del usuario es "+email);

    userSrv.findUserByEmail(email, function (err, user) {
       if (err) return res.status(500).send({"mensaje" : "Servicio no disponible en este momento."});
       if (!user) return res.status(401).send({"mensaje" : "Login incorrecto"});

       //Comprobacion password introducido
       if(crypt.checkPassword(pass, user.password) === true) {
          var queryUpdate = '{"$set":{"logged":true}}';
          userSrv.updateUser(user._id, queryUpdate, function (err, userUpdated) {
            if (err) return res.status(500).send({"mensaje" : "Servicio no disponible en este momento."});
            if (!userUpdated) return res.status(401).send({"mensaje" : "Error en el acceso del usuario"});

            //Actualizado flag logged a true. Generamos el token
            var token = crypt.generateToken(user._id);
            var userLogged = {
              id : user._id,
              email : user.email,
              token : token
            };
            //var response = {"mensaje":"Login correcto", "idUsuario":user.id};
            //Añado el token al header
            //res.setHeader('tsec', token);
            return res.status(200).send(userLogged);
          });
       } else {
         //Password erronea
         return res.status(401).send({"mensaje" : "Login incorrecto"});
       }
     }
   );
}

function logoutV1(req, res){
  console.log("POST /charrobank/v1/logout");
  var userId = Number.parseInt(req.params.id);
  var userIdToken = Number.parseInt(req.userIdToken);
  console.log("getUserByIdV1.- userId url: "+userId+"- userIdToken:"+userIdToken);
  if(userIdToken != userId){
    console.log("getUserByIdV1.- ID USUARIO req.params.id NO ES EL MISMO QUE ");
    return res.status(422).send({"mensaje" : "No tiene permisos para realizar la operacion."});
  }

  userSrv.findUserById(userId, function (err, user) {
     if (err) return res.status(500).send({"mensaje" : "Servicio no disponible en este momento."});
     if (!user) return res.status(401).send({"mensaje" : "Usuario no existe"});

     var queryUpdate = '{"$unset":{"logged":""}}';
     userSrv.updateUser(user._id, queryUpdate, function (err, userUpdated) {
       if (err) return res.status(500).send({"mensaje" : "Servicio no disponible en este momento."});
       if (!userUpdated) return res.status(401).send({"mensaje" : "Error en el acceso del usuario"});
       return res.status(200).send({"mensaje" : "Logout correcto"});
     });
  });
}
module.exports.loginV1 = loginV1;
module.exports.logoutV1 = logoutV1;
