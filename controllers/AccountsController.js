const utils = require('../utils/Utils');

const accountSrv = require('../services/AccountsService');
const transactionSrv = require('../services/TransactionsService');

function createAccountV1(req, res){
    console.log("POST /charrobank/v1/accounts");
    console.log("createAccountV1.- req.body", req.body);
    console.log("createAccountV1.- JSON.stringify({req.body})", JSON.stringify(req.body));

    // Validar que vienen los campos necesarios para crear una nueva cuenta
    if (!req.userIdToken) {
       return res.status(400).send({"mensaje" : "Campos obligatorios no informados"});
    }
    console.log("El id del usuario es "+req.userIdToken);
    var newAccount = {
      //iban: bankHelpers.generateIbanForBank('41189'),
      user_id: req.userIdToken,
      balance : 0.00,
      currency : "EUR",
      openingDate: new Date(),
      status: "ACTIVA"
   };
   /*if(req.body){
     newAccount.balance = Number.parseFloat(req.body.balance);
     newAccount.currency = req.body.currency;
   }else{
     newAccount.balan;ce = 0.00;
     newAccount.currency = "EUR";
   }*/
   //Creamos la nueva cuenta
   accountSrv.createAccount(newAccount, function (err, account) {
    if (err || !account) return res.status(500).send({"mensaje" : "Servicio no disponible en este momento."});
    console.log('Cuenta nueva creada:', account);
    //Añado el token al header
    //res.setHeader('tsec', token);
    return res.status(201).send(account);
  });
}

function getAccountsByUserIdV1(req, res){
  console.log("GET getAccountsUserV1 /charrobank/v1/accounts");

  accountSrv.findAccountsByUserId(req.userIdToken, function (err, accounts) {
     if (err) return res.status(500).send({"mensaje" : "Servicio no disponible en este momento."});
     if (!accounts) return res.status(204).send({"mensaje" : "Usuario sin cuentas"});
     console.log("LISTADO DE CUENTAS RECUPERADAS:", accounts);
     return res.status(200).send(accounts);
  });
}

function getAccountByIdV1(req, res){
  console.log("GET getAccountByIdV1 /charrobank/v1/accounts/:id");
  console.log("getAccountByIdV1.- accountId: "+req.params.id);
  accountSrv.findAccountById(Number.parseInt(req.params.id), function (err, account) {
     if (err) return res.status(500).send({"mensaje" : "Servicio no disponible en este momento."});
     if (!account) return res.status(204).send({"mensaje" : "La cuenta no existe"});

     var userIdToken = Number.parseInt(req.userIdToken);
     var userIdAccount = Number.parseInt(account.user_id);
     if(userIdToken != userIdAccount){
       console.log("getAccountByIdV1.- ID USUARIO logado NO ES EL MISMO QUE el de la cuenta. ");
       return res.status(422).send({"mensaje" : "No tiene permisos para realizar la operacion."});
     }
     console.log("CUENTA RECUPERADA:", account);
     //Recuperamos los mov asociados a la cuenta
     transactionSrv.findTransactionsByAccountId(Number.parseInt(req.params.id), function (err, transactions) {
       if (err) return res.status(500).send({"mensaje" : "Servicio no disponible en este momento. Intentelo de nuevo pasados unos minutos"});
       if (!transactions){
         account.transactions = [{}];
       }
       account.transactions = transactions;
       return res.status(200).send(account);
     });
  });
}

function removeAccountV1(req, res){
  console.log("DELETE removeAccountV1 /charrobank/v1/accounts/:id");
  console.log("removeAccountV1.- accountId: "+req.params.id);
  accountSrv.findAccountById(Number.parseInt(req.params.id), function (err, account) {
     if (err) return res.status(500).send({"mensaje" : "Servicio no disponible en este momento."});
     if (!account) return res.status(422).send({"mensaje" : "La cuenta no existe"});

     var userIdToken = Number.parseInt(req.userIdToken);
     var userIdAccount = Number.parseInt(account.user_id);
     if(userIdToken != userIdAccount){
       console.log("getAccountByIdV1.- ID USUARIO logado NO ES EL MISMO QUE el de la cuenta. ");
       return res.status(422).send({"mensaje" : "No tiene permisos para realizar la operacion."});
     }
     console.log("CUENTA RECUPERADA:", account);
     if(Number.parseFloat(account.balance) != 0){
       console.log("CUENTA CON SALDO. NO SE PUEDE BORRAR:", account);
       return res.status(409).send({"mensaje" : "El sado de la cuenta tiene que ser 0 para que se pueda eliminar. Saldo actual:"+account.balance});
     }else{
       //Borramos la cuenta con saldo 0
       accountSrv.deleteAccount(account._id, function (err, deletedAccount) {
         if (err || !deletedAccount) return res.status(500).send({"mensaje" : "Servicio no disponible en este momento."});
         console.log('Cuenta borrada:', deletedAccount);
         //Borramos los movimientos asociados a la cuenta eliminada
         transactionSrv.deleteTransactions(account._id, function (err, deletedTransactions) {
           if (err || !deletedTransactions) return res.status(500).send({"mensaje" : "Servicio no disponible en este momento."});
           console.log('Movimientos borrados:', deletedTransactions);
           var removedElements = {
             removedAccounts: 1,
             removedTransactions: deletedTransactions.removed
           };
           return res.status(200).send(removedElements);
         });
       });
     }
  });
}

module.exports.createAccountV1 = createAccountV1;
module.exports.getAccountsByUserIdV1 = getAccountsByUserIdV1;
module.exports.getAccountByIdV1 = getAccountByIdV1;
module.exports.removeAccountV1 = removeAccountV1;
