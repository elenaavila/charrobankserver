const requestJson = require('request-json');

const mLabUrlBD = process.env.MLAB_API_URL_BD;
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
const sequencesCollection = "sequences";

function nextValSequence(collectionName, callback){
  console.log("nextValSequences.- collectionName: ", collectionName);
  var objectSequences = {
    findAndModify: sequencesCollection,
    query:  {_id: collectionName},
    update: { $inc: { seq: 1 } },
    upsert: true,
    new: true
  };
  var httpClient = requestJson.createClient(mLabUrlBD);
  var url = "runCommand?" + mLabAPIKey;
  console.log("nextValSequences.- url: ", url);
  console.log("nextValSequences.- objectSequences: ", objectSequences);
  httpClient.post(url, objectSequences,
      function(err, resMLab, body) {
        if (err) {
          console.log("nextValSequences ERROR Mlab:", err);
          return callback(err);
        } else if(!body.value){
          console.log("nextValSequences body.value null");
          return callback(null, null);
        }else{
          console.log("nextValSequences respuesta body:", body);
          console.log("nextValSequences respuesta body.value.seq:", body.value.seq);
          return callback(err, body.value.seq);
        }
      }
   );
}

function validateUserFields(user){
  if(    !user.name || !user.first_name || !user.last_name
      || !user.email || !user.password){
    return false;
  }
  return true;
}

function validateAccountFields(account){
  if(account){
    console.log("validateAccountFields account viene informada, validamos sus campos:", account);
    console.log("account.currency", account.currency);
    console.log("account.balance", account.balance);

    if(!account.currency || account.balance===undefined){
      console.log("validateAccountFields NO VIENE INFORMAOD currency o balance");
      return false;
    }
  }
  return true;
}

function validateTransactionFields(transaction){
  if(    !transaction.amount || !transaction.currency
      || !transaction.type || !transaction.description){
    return false;
  }
  return true;
}

module.exports.nextValSequence = nextValSequence;
module.exports.validateUserFields = validateUserFields;
module.exports.validateAccountFields = validateAccountFields;
module.exports.validateTransactionFields = validateTransactionFields;
