const requestJson = require('request-json');
const bankHelpers = require('iban-constructor/lib/bankHelpers')

const utils = require('../utils/Utils');

const mLabUrl = process.env.MLAB_API_URL;
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
const mLabUrlBD = process.env.MLAB_API_URL_BD;

function createAccount(account, callback) {
  utils.nextValSequence("accounts", function (err, numIndex) {
     if (err) return callback(err);
     if (!numIndex) return callback(err, numIndex);
     //Insertamos la nueva cuenta con el siguiente "id" de la secuencia accounts
     account._id = numIndex;
     if(!account.iban){
       //Si no viene informado el iban, generamos uno aleatorio de BBVA
       account.iban = bankHelpers.generateIbanForBank('41189');
     }
     console.log("en createAccount FIN OK nextValSequence.- account:", account);
     var httpClient = requestJson.createClient(mLabUrl);
     var url = "accounts?" + mLabAPIKey;
     console.log("createAccount.- URL: ", url);
     httpClient.post(url, account,
       function(err, resMlab, body){
         console.log("createAccount.- Cuenta creada");
         if(err) return callback(err);
         console.log("createAccount.-body:", body);
         return callback(err, body);
     });
  });
}

function findAccountsByUserId(userId, callback) {
    console.log("findAccountsByUserId.- ", userId);
  var httpClient = requestJson.createClient(mLabUrl);
  var query = "q=" + JSON.stringify({"user_id": userId});
  var url = "accounts?" + query + "&" + mLabAPIKey;
  console.log("findAccountsByUserId.- URL: ", url);
  httpClient.get(url,
    function (err, resMLab, body) {
      if (err) {
        console.log("findAccountsByUserId.- Error en el acceso a base de datos", err);
        return callback(err);
      } else {
        if (body.length > 0) {
          console.log("findAccountsByUserId.- Cuentas encontradas:", body);
          return callback(err, body);
        } else {
          console.log("findAccountsByUserId.- Cuentas no encontradas para el usuario", userId);
          return callback(null, null);
        }
      }
    }
  );
}

function findAccountById(accountId, callback) {
  var httpClient = requestJson.createClient(mLabUrl);
  var query = "q=" + JSON.stringify({"_id": accountId});
  var url = "accounts?" + query + "&" + mLabAPIKey;
  console.log("findAccountById.- URL: ", url);
  httpClient.get(url,
    function (err, resMLab, body) {
      if (err) {
        console.log("findAccountById.- Error en el acceso a base de datos", err);
        return callback(err);
      } else {
        if (body.length > 0) {
          var account = body[0];
          console.log("findAccountById.- Cuenta encontrada", account);
          return callback(err, account);
        } else {
          console.log("findAccountById.- account NO encontrada", account);
          return callback(null, null);
        }
      }
    }
  );
}

function deleteAccount(accountId, callback){
  var queryId = "q=" + JSON.stringify({"_id": Number.parseInt(accountId)});
  console.log("deleteAccount.- QUERY ID: " + queryId);
  var httpClient = requestJson.createClient(mLabUrl);
  httpClient.put("accounts?" + queryId + "&" + mLabAPIKey, [],
    function(err, resMlab, body){
      if(err) return callback(err);
      console.log("deleteAccount.-body:", body);
      var deletedAccount = {
        id:accountId
      };
      return callback(err, deletedAccount);
  });
}


function updateAccount(account, callback){
  var queryId = "q=" + JSON.stringify({"_id": Number.parseInt(account._id)}) + "&u=false";
  console.log("updateAccount.- QUERY ID: " + queryId);
  var httpClient = requestJson.createClient(mLabUrl);
  httpClient.put("accounts?" + queryId + "&" + mLabAPIKey, account,
    function(err, resMlab, body){
      if(err) return callback(err);
      console.log("updateAccount.-body:", body);
      return callback(err, account);
  });
}

module.exports.createAccount = createAccount;
module.exports.findAccountsByUserId = findAccountsByUserId;
module.exports.findAccountById = findAccountById;
module.exports.deleteAccount = deleteAccount;
module.exports.updateAccount = updateAccount;
