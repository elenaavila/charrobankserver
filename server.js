require('dotenv').config();
const express = require('express');
const app = express();
const jwt = require('jsonwebtoken');

const authController = require('./controllers/AuthController');
const usersController = require('./controllers/UsersController');
const accountsController = require('./controllers/AccountsController');
const transactionsController = require('./controllers/TransactionsController');
const userSrv = require('./services/UsersService');

const jwtSecret = process.env.JWT_SECRET;

var enableCORS = function(req, res, next) {
 res.set("Access-Control-Allow-Origin", "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
 res.set("Access-Control-Allow-Headers", "Content-Type, x-access-tokensec");
 next();
}
//Parsea los body de las peticiones a json
app.use(express.json());
app.use(enableCORS);

//Se levanta la API en el puerto definido o el 3000 por defecto
const port = process.env.PORT || 3000;
app.listen(port);
console.log("API Charro Bank escuchando en el puerto:"+port);

app.use(function (req, res, next) {
  console.log('Funcion Middleware:', Date.now());
  console.log("Middleware.- REQ URL req.url", req.url);
  console.log("Middleware.- REQ METHOD req.method", req.method);
  console.log(req.headers);
  if (   '/charrobank/v1/login' === req.url
      || '/charrobank/_hello' === req.url
      || '/charrobank/v1/enrollment' === req.url
      || req.method === 'OPTIONS') {
    next();
  }else{
    var token = req.headers['x-access-tokensec'];
    console.log("Middleware.- token HEADER:", token);
    if (!token) {
        console.log("NO VIENE TOKEN INFORMADO. ERROR");
        //return res.status(403).send({ auth: false, mensaje: 'Usuario no autenticado.' });
        return res.status(403).send({"mensaje": "Usuario no autenticado."});
    } else {
      jwt.verify(token, jwtSecret, function(err, decoded) {
        if(err){
          console.log("checkToken err:", err);
          return res.status(403).send({"mensaje": "Usuario no autenticado."});
        }else{
          req.userIdToken = decoded.id;
          userSrv.findUserById(decoded.id, function (err, userFound) {
            if (err) return res.status(500).send({"mensaje" : "Servicio no disponible en este momento."});
            if (!userFound) return res.status(401).send({"mensaje" : "Error en el acceso del usuario"});
            console.log("checkToken userFound:", userFound);
            if(userFound.logged === true){
              console.log("checkToken . logged true - decoded.id:", decoded.id);
              // Si el token es correcto, devolvemos el id del usuario logado
              next();
            }else{
              return res.status(403).send({"mensaje": "Usuario no autenticado."});
            }
          });
        }
      });
    }
  }
});

// GET para comporbar que el API arranca correctamente
app.get('/charrobank/_hello', function (req, res) {
  console.log("GET /charrobank/_hello");
  res.status(200).send({"mensaje" : "API Charro Bank Funcionando!!"});
});

//Methods AuthController (login , logout)
app.post("/charrobank/v1/login", authController.loginV1);
app.post("/charrobank/v1/logout/:id", authController.logoutV1);
//Methods UsersController (crear usuario, detalle usuario por id)
app.post('/charrobank/v1/enrollment', usersController.enrollmentV1);
app.get('/charrobank/v1/users/:id', usersController.getUserByIdV1);
//Methods AccountController (crear cuenta , listar cuentas, detalle cuenta)
app.post('/charrobank/v1/accounts', accountsController.createAccountV1);
app.get('/charrobank/v1/accounts', accountsController.getAccountsByUserIdV1);
app.get('/charrobank/v1/accounts/:id', accountsController.getAccountByIdV1);
app.delete('/charrobank/v1/accounts/:id', accountsController.removeAccountV1);
//Methods TransactionsController (crear mov , listar movimientos cuenta)
app.post('/charrobank/v1/accounts/:id/transactions', transactionsController.createTransactionV1);
app.get('/charrobank/v1/accounts/:id/transactions', transactionsController.getTransactionsByAccountIdV1);
app.delete('/charrobank/v1/accounts/:id/transactions', transactionsController.deleteTransactionsByAccountIdV1);
