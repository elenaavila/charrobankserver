# Base image
FROM node

# Root Folder
WORKDIR /charrobank

# Copy local files
ADD . /charrobank

#Install package.json dependencies
RUN npm install

# Listen port
EXPOSE 3000

# Start command
CMD ["npm", "start"]